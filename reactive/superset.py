from charms.reactive import when, when_not, set_flag
from subprocess import check_call, Popen
from charmhelpers.core.hookenv import resource_get, status_set, log, open_port
import charms.apt

@when_not('apt.installed.build-essential')
def install():
    charms.apt.queue_install(['build-essential', 'libssl-dev','libffi-dev','python-dev','python-pip','libsasl2-dev','libldap2-dev','python3.5-dev'])


@when_not('superset.installed')
@when('apt.installed.build-essential')
def install_superset():
    open_port('8088')
    check_call(['pip2', 'install', 'markdown==2.6.11'])
    check_call(['pip2', 'install', 'superset==0.27.0'])
    check_call(['fabmanager', 'create-admin', '--app', 'superset', '--username', 'admin', '--firstname', 'admin', '--lastname', 'user', '--email', 'info@example.com', '--password', '12345'])
    check_call(['superset', 'db', 'upgrade'])
    check_call(['superset', 'load_examples'])
    check_call(['superset', 'init'])
    set_flag('superset.installed')
    status_set('waiting','Waiting to start')

@when('superset.installed')
@when_not('superset.started')
def start_superset():
    check_call(['./scripts/launch_superset'])
    set_flag('superset.started')
    status_set('active','Superset running')
